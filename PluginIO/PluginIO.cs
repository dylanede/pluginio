﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Reflection;
namespace PluginIO
{
    [KSPAddon(KSPAddon.Startup.Instantly, true)]
    public class PluginIO : MonoBehaviour
    {
        private bool done = false;
        public void OnGUI()
        {
            if(!done)
                runOnce();
        }
        private void runOnce()
        {
            Type Path = Type.GetType("System.IO.Path");
            Func<string, string> getdir = (string p) => (string)Path.GetMethod("GetDirectoryName", BindingFlags.Static | BindingFlags.Public).Invoke(null, new object[] { p });
            Func<string, string, string>  combine_dir = (string a, string b) => (string)Path.GetMethod("Combine", BindingFlags.Static | BindingFlags.Public).Invoke(null, new object[] { a, b });

            string local_url = getdir(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string local_dir = local_url.Substring(6);
            string gd_path = getdir(local_dir);

            //Load the utility assembly that contains the code for finding unloaded dlls
            Assembly Util = Assembly.LoadFrom(combine_dir(local_dir, "PluginIOUtil.dll"));
            IUtil util = (IUtil)Util.GetType("PluginIOUtil.Util").GetConstructor(System.Type.EmptyTypes).Invoke(null);
            string[] toload = util.getUnloadedLibraryPaths(gd_path);
            int count = toload.Length;
            foreach (string path in toload)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFrom(path);

                    //make sure that types from the assembly are added to the correct lists that the game looks through when loading parts, part modules etc.
                    AssemblyLoader.LoadedAssembly loadedAssembly = new AssemblyLoader.LoadedAssembly(assembly, path, assembly.GetName().CodeBase);
                    foreach (Type type in assembly.GetTypes())
                        foreach (Type t in AssemblyLoader.loadedTypes)
                            if (type.IsSubclassOf(t) || type == t)
                                loadedAssembly.types.Add(t, type);
                    AssemblyLoader.loadedAssemblies.Add(loadedAssembly);

                    //Manually start any KSPAddon behaviours that should start "Instantly"
                    foreach (Type type in assembly.GetTypes())
                    {
                        if (type.IsSubclassOf(typeof(MonoBehaviour)))
                        {
                            KSPAddon[] spec = (KSPAddon[])type.GetCustomAttributes(typeof(KSPAddon), true);
                            if (spec.Length > 0 && spec[0].startup == KSPAddon.Startup.Instantly)
                            {
                                GameObject go = new GameObject("addon_" + type.Name);
                                go.AddComponent(type);
                            }
                        }
                    }
                    Debug.Log("PluginIO: \"" + path + "\" loaded");
                }
                catch (Exception e)
                {
                    Debug.Log("PluginIO: \"" + path + "\" could not be loaded, exception:");
                    Debug.Log(e.ToString());
                    count--;
                }
            }
            Debug.Log("PluginIO: All " + count + " compatible libraries using IO loaded.");
            done = true;
        }
    }
}
