﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

namespace PluginIOUtil
{
    public class Util : PluginIO.IUtil
    {
        public string[] getUnloadedLibraryPaths(string root)
        {
            HashSet<string> allPaths = new HashSet<string>();
            allPaths.UnionWith(Directory.GetFiles(root, "*.dll", SearchOption.AllDirectories));

            HashSet<string> loadedNames = new HashSet<string>();
            foreach (AssemblyLoader.LoadedAssembly loaded in AssemblyLoader.loadedAssemblies)
            {
                loadedNames.Add(Path.GetFileName(loaded.path));
            }

            return allPaths.Where(p => loadedNames.Where(n => p.EndsWith(n)).Count() == 0 && !p.Contains("PluginIO")).ToArray();
        }
    }
}
